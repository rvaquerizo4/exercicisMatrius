let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}

/*
matrix = 
[[1,2,3,5,6],
 [4,5,6,7,8],
 [7,8,9,1,3]]
 */

function paintAll() {
    erase();
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j <matrix[0].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j < matrix[0].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}


function paintRightHalf() {
    erase();
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j < matrix[0].length; j++ ) { // afegir codi
            if(j>matrix[0].length/2)
                matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintLeftHalf() {
    erase();
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j < (matrix[0].length)/2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {
    erase();

    for (let i=0; i < matrix.length/2; i++) { // afegir codi
        for (let j=0; j < matrix[0].length; j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();
    
    for (var i=0; i < matrix.length; i++) { // afegir codi
        for (var j=0; j < i; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperTriangle() {
    erase();
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j < matrix[0].length-i; j++) { // afegir codi
            // afegir codi
                matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i=0; i < matrix.length; i++) { // afegir codi
        for (let j=0; j < matrix[0].length; j++) { // afegir codi
            if(j<1)
            matrix[i][j].style.backgroundColor = "red";
            if(j==matrix[0].length-1)
            matrix[i][j].style.backgroundColor = "red";
            if(i<1)
            matrix[i][j].style.backgroundColor = "red";
            if(i==matrix[0].length)
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintCheckerboard() {
    erase();
    for (let i=0; i < matrix.length; i++) {
        for (let j=0; j < matrix[0].length; j++) {
            if((j%2) == 0 && (i%2) == 0)
            matrix[i][j].style.backgroundColor = "red";
            else if((j%2) != 0 && (i%2) != 0)
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}


function paintCheckerboard2() {
    erase();
    for (let i=0; i < matrix.length; i++) {
        for (let j=0; j <matrix[0].length; j++) { 
            if(i%2!=j%2)
            matrix[i][j].style.backgroundColor = "red";     
        }
    }
}


function paintNeighbours() {
    erase();
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    for (let i=-1;i<=1;i++){
        for (let j=-1;j<=1;j++ ){
            if (!(i==0 && j==0)){
                matrix[inputX+i][inputY+j].style.backgroundColor="red";
            }
        }
    }
}


function countNeighbours(x,y) {
    let count = 0;
    for (i=-1; i<=1; i++){
        for (j=-1; j<=1; j++){
            let aux1=i+x;
            let aux2=j+y;
            if((aux1<matrix.length && aux1>0) && (aux2<matrix[aux1].length && aux2>0)){               
                if (matrix[aux1][aux2].style.backgroundColor=="red"){
                    if(aux1!=x){
                    ++count;
                    }
                    else if(aux2!=y)
                    ++count;
            }
            }
        }
    }
    return count;
}


function paintAllNeighbours() {
    for(let i=0; i<matrix.length; i++){
        for(let j=0; j<matrix[i].length; j++){
	        matrix[i][j].innerText = count;
	    }
    }
}

